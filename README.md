# Event Resources

Resources (slides, software, etc.) to go with ASULUG events and presentations

## Spring 2019

- 1/23 - [What is Linux and How Do I Get It Onto My Computer?](https://gitlab.com/asulug/resources/-/tree/2019-Spring/01-23_What-is-Linux)
- 1/30 - [Introduction to the Command Line Interface](https://gitlab.com/asulug/resources/-/tree/2019-Spring/01-30_Introduction-to-CLI)

## Spring 2020

- 2/3 - [Introduction to Linux](https://gitlab.com/asulug/resources/-/tree/2020-Spring/02-03_Introduction-to-Linux)
- 2/17 - [GUIs with PyQt: An Introductory Walkthrough *(Canceled)*](https://gitlab.com/asulug/resources/-/tree/2020-Spring/02-17_GUIs-with-PyQt)
- 4/17 - [LaTeX Introductory Walkthrough](https://gitlab.com/asulug/resources/-/tree/2020-Spring/04-17_LaTeX-Introductory-Walkthrough)

## Fall 2020

- 8/26 - [Homelab Series Primer: What Hardware to Get](https://gitlab.com/asulug/resources/-/tree/2020-Fall/08-26_Homelab-Hardware)
- 9/25 - [Virtualizing with XCP-NG](https://gitlab.com/asulug/resources/-/tree/2020-Fall/09-25_XCP-NG)

## Spring 2021

- 2/2 - [Neovim Introductory Walkthrough](https://gitlab.com/asulug/resources/-/tree/2021-Spring/02-02_Neovim-Introductory-Walkthrough)
